package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {
	//test comment 2
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
